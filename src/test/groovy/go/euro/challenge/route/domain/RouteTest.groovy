package go.euro.challenge.route.domain

import spock.lang.Specification
import spock.lang.Unroll

class RouteTest extends Specification {

    def "should throw exception when id is not positive number"() {
        when:
        BusLine.route(-1, [])

        then:
        thrown(IllegalArgumentException)
    }

    @Unroll
    def "has direct connection when departs from #departId to #arrivalId and route goes on stops #stops"() {
        when:
        def route = BusLine.route(1, stops)

        then:
        route.hasDirectConnection(departId, arrivalId)

        where:
        departId | arrivalId | stops
        1        | 2         | [1, 3, 2]
        2        | 3         | [1, 2, 3]
        1        | 1000      | (1..1000)
    }

    @Unroll
    def "has not direct connection when departs from #departId to #arrivalId and route goes on stops #stops"() {
        when:
        def route = BusLine.route(1, stops)

        then:
        !route.hasDirectConnection(departId, arrivalId)

        where:
        departId | arrivalId | stops
        2        | 1         | [1, 3, 2]
        3        | 2         | [1, 2, 3]
        1000     | 1         | (1..1000)
        1        | 5         | [1, 2, 3, 4]
    }

    @Unroll
    def "should throw exception when invalid stops list passed"(List<Integer> stops, String message) {
        when:
        BusLine.route(1, stops)

        then:
        def exception = thrown(IllegalArgumentException)
        exception.message == message

        where:
        stops           || message
        null            || "'stops' should not be null"
        []              || "'stops' should be more than 2"
        (1..1001)       || "'stops' should be less than 1001"
        [1, 1, 2, 3, 4] || "No unique elements of route stops with id: 1"
    }

    @Unroll
    def "should create route when valid id and stops list passed"(List<Integer> stops) {
        expect:
        BusLine.route(1, stops)

        where:
        stops << [[1, 2, 3], (1..1000)]
    }
}
