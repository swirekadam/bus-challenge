package go.euro.challenge.route.domain;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.ImmutableList.of;
import static com.google.common.collect.Sets.newHashSet;

public class BusLine {

    public static final int MIN_ROUTE_STOPS = 3;
    public static final int MAX_ROUTE_STOPS = 1000;

    private final int id;
    private final List<Integer> stops;

    private BusLine(int id, List<Integer> stops) {
        checkArgument(id > 0, "'id' should be greater than 0");
        checkArgument(stops != null, "'stops' should not be null");
        checkArgument(stops.size() >= MIN_ROUTE_STOPS, "'stops' should be more than " + (MIN_ROUTE_STOPS - 1));
        checkArgument(stops.size() <= MAX_ROUTE_STOPS, "'stops' should be less than " + (MAX_ROUTE_STOPS + 1));
        checkArgument(stops.size() == newHashSet(stops).size(), "No unique elements of route stops with id: " + id);
        this.id = id;
        this.stops = copyOf(stops);
    }

    public static BusLine route(int id, List<Integer> stops) {
        return new BusLine(id, stops);
    }

    public List<Integer> getStops() {
        return stops;
    }

    public int getId() {
        return id;
    }

    public boolean hasDirectConnection(int departId, int arrivalId) {
        return stops.containsAll(of(departId, arrivalId)) && arrivalIdAfterDepartId(departId, arrivalId);
    }

    private boolean arrivalIdAfterDepartId(int departId, int arrivalId) {
        return stops.indexOf(departId) < stops.indexOf(arrivalId);
    }
}
