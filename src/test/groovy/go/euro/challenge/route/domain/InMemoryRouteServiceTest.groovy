package go.euro.challenge.route.domain

import spock.lang.Specification
import spock.lang.Unroll

import static BusLine.route

class InMemoryRouteServiceTest extends Specification {

    def "should return no direct route when rides from 1 to 2 and route stops are: #stops"() {
        given:
        def departId = 1
        def arrivalId = 2
        DirectRouteService service = new InMemoryDirectRouteService(makeRoute([1, 3, 5]))

        when:
        Route route = service.findDirectConnection(departId, arrivalId)

        then:
        !route.directBusRoute
    }

    @Unroll
    def "should return no direct route when ride from 1 to 2 and lines are #lines"() {
        given:
        DirectRouteService service = new InMemoryDirectRouteService(makeRoutes(lines))

        when:
        Route route = service.findDirectConnection(departId, arrivalId)

        then:
        route
        !route.directBusRoute

        where:
        lines                  | _
        [[2, 3, 1, 5]]         | _
        [[2, 3, 5], [5, 2, 1]] | _
        [[1, 3, 5], [5, 3, 2]] | _

        departId = 1
        arrivalId = 2
    }

    @Unroll
    def "should found direct route when ride from 1 to 3 and stops are #stops"() {
        given:
        DirectRouteService service = new InMemoryDirectRouteService(makeRoute(stops))

        when:
        def route = service.findDirectConnection(departId, arrivalId)

        then:
        route.directBusRoute == true

        where:
        stops           | _
        [1, 3, 4]       | _
        [1, 5, 6, 3]    | _
        [6, 7, 8, 1, 3] | _

        departId = 1
        arrivalId = 3
    }

    def static List<BusLine> makeRoute(List<Integer> stops) {
        makeRoutes([stops])
    }

    def static List<BusLine> makeRoutes(List<List<Integer>> stops) {
        stops.collect { route(it.hashCode(), it) }
    }
}
