package go.euro.challenge.route.api;

import go.euro.challenge.route.domain.DirectRouteService;
import go.euro.challenge.route.domain.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class SearchController {

    private final DirectRouteService directRouteService;

    @Autowired
    public SearchController(DirectRouteService directRouteService) {
        this.directRouteService = directRouteService;
    }

    @RequestMapping(value = "/api/direct", method = GET)
    @ResponseBody
    public Route searchDirect(@Valid SearchRequest request) {
        return directRouteService.findDirectConnection(request.getDepSid(), request.getArrSid());
    }
}
