package go.euro.challenge;

import go.euro.challenge.route.domain.BusLine;
import go.euro.challenge.route.parser.BusFileParser;
import go.euro.challenge.route.parser.StrictBusFileParser;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;

import java.io.File;
import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration
public class GoEuroBusRouteChallenge {
    public static void main(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("File path should be passed as app argument");
        }

        BusFileParser busFileParser = new StrictBusFileParser();
        final List<BusLine> busLines = busFileParser.parse(new File(args[0]));

        new SpringApplicationBuilder()
                .listeners(new RegisterBusLines(busLines))
                .sources(GoEuroBusRouteChallenge.class)
                .run(args);
    }

    private static class RegisterBusLines implements ApplicationListener<ApplicationPreparedEvent> {

        private final List<BusLine> busLines;

        RegisterBusLines(List<BusLine> busLines) {
            this.busLines = busLines;
        }

        @Override
        public void onApplicationEvent(ApplicationPreparedEvent event) {
            event.getApplicationContext().getBeanFactory().registerSingleton("busLines", busLines);
        }
    }
}
