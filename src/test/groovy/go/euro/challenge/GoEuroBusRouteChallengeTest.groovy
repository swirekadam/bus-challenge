package go.euro.challenge

import go.euro.challenge.route.config.TestConfig
import go.euro.challenge.route.domain.Route
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@ContextConfiguration
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [TestConfig, GoEuroBusRouteChallenge])
class GoEuroBusRouteChallengeTest extends Specification {

    @Autowired
    TestRestTemplate restTemplate

    def "should return direct route"() {
        when:
        Route result = restTemplate.getForObject("/api/direct?dep_sid=121&arr_sid=114", Route.class)

        then:
        with(result) {
            it.directBusRoute == true
            it.depSid == 121
            it.arrSid == 114
        }
    }

    def "should return no direct route"() {
        when:
        Route result = restTemplate.getForObject("/api/direct?dep_sid=121&arr_sid=115", Route.class)

        then:
        with(result) {
            it.directBusRoute == false
            it.depSid == 121
            it.arrSid == 115
        }
    }
}
