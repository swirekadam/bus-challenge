package go.euro.challenge.route.domain;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.List;

import static java.util.stream.IntStream.range;

public class InMemoryDirectRouteService implements DirectRouteService {

    private final Multimap<Integer, Integer> directRoutes;

    public InMemoryDirectRouteService(List<BusLine> busLines) {
        this.directRoutes = prepareDirectConnection(busLines);
    }

    public Route findDirectConnection(int departId, int arrivalId) {
        boolean directRoute = directRoutes.get(departId).contains(arrivalId);
        return Route.route(departId, arrivalId, directRoute);
    }

    private Multimap<Integer, Integer> prepareDirectConnection(List<BusLine> busLines) {
        final Multimap<Integer, Integer> directs = ArrayListMultimap.create();

        busLines.forEach(busLine -> addDirectConnections(directs, busLine));

        return directs;
    }

    private void addDirectConnections(Multimap<Integer, Integer> directs, BusLine busLine) {
        int size = busLine.getStops().size();
        range(0, size).forEach(departIdx -> {
            range(departIdx + 1, size).forEach(arrivalIdx -> {
                addConnectionIfNotExists(directs, busLine, departIdx, arrivalIdx);
            });
        });
    }

    private void addConnectionIfNotExists(Multimap<Integer, Integer> directs, BusLine busLine, int departIdx, int arrivalIdx) {
        if (connectionDoesNotExists(directs, busLine, departIdx, arrivalIdx)) {
            directs.put(busLine.getStops().get(departIdx), busLine.getStops().get(arrivalIdx));
        }
    }

    private boolean connectionDoesNotExists(Multimap<Integer, Integer> directs, BusLine busLine, int departIdx, int arrivalIdx) {
        return !directs.get(busLine.getStops().get(departIdx)).contains(busLine.getStops().get(arrivalIdx));
    }
}
