package go.euro.challenge.route.parser;


import go.euro.challenge.route.domain.BusLine;

import java.io.File;
import java.util.List;

public interface BusFileParser {
    List<BusLine> parse(File file);
}
