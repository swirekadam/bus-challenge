package go.euro.challenge.route.parser;

import static java.lang.String.format;

public class InvalidFormatException extends RuntimeException {

    public InvalidFormatException(String message) {
        super(message);
    }

    public InvalidFormatException(String path, Throwable cause) {
        super(format("File %s has invalid format", path), cause);
    }
}
