package go.euro.challenge.route.parser

import go.euro.challenge.route.domain.BusLine
import spock.lang.Specification
import spock.lang.Unroll

class StrictBusFileParserTest extends Specification {

    StrictBusFileParser busFileParser = new StrictBusFileParser()

    def "should throw exception when cannot read file"() {
        when:
        busFileParser.parse(new File("does/not/exists"))

        then:
        thrown(IOFileException)
    }

    @Unroll
    def "should throw illegal file format when '#filename' file passed"(String filename) {
        when:
        busFileParser.parse(new File("src/test/resources/${filename}.dat"))

        then:
        thrown(InvalidFormatException)

        where:
        filename                           || _
        "wrong-header-not-a-number"        || _
        "wrong-route-data-id-not-a-number" || _
        "wrong-route-stop-id-not-a-number" || _
        "wrong-route-format"               || _
        "more-routes-than-declared"        || _
        "less-routes-than-declared"        || _
        "duplicate-route-id"               || _
    }

    def "should parse valid file"() {
        when:
        List<BusLine> result = busFileParser.parse(new File("src/test/resources/valid.dat"))

        then:
        result.size() == 3

        and:
        with(result[0]) {
            it.id == 1
            it.stops == [2, 3, 4, 5]
        }

        and:
        with(result[1]) {
            it.id == 2
            it.stops == [3, 4, 5, 6]
        }

        and:
        with(result[2]) {
            it.id == 3
            it.stops == [4, 5, 6, 7]
        }
    }
}
