package go.euro.challenge.config;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import go.euro.challenge.route.domain.BusLine;
import go.euro.challenge.route.domain.DirectRouteService;
import go.euro.challenge.route.domain.InMemoryDirectRouteService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.util.List;

@Configuration
public class AppConfig {

    @Bean
    DirectRouteService directRouteService(List<BusLine> busLines) {
        return new InMemoryDirectRouteService(busLines);
    }

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        return Jackson2ObjectMapperBuilder.json()
                .propertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
}
