package go.euro.challenge.route.config

import go.euro.challenge.route.domain.BusLine
import go.euro.challenge.route.parser.BusFileParser
import go.euro.challenge.route.parser.StrictBusFileParser
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TestConfig {

    @Bean
    List<BusLine> busLines() {
        BusFileParser busFileParser = new StrictBusFileParser()
        return busFileParser.parse(new File("src/test/resources/example"))
    }
}
