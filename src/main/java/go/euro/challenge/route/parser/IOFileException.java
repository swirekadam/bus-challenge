package go.euro.challenge.route.parser;

import java.io.IOException;

public class IOFileException extends RuntimeException {

    public IOFileException(String absolutePath, IOException e) {
        super(String.format("IO exception occurs while trying to read file %s", absolutePath), e);
    }
}
