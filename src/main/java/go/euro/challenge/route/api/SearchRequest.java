package go.euro.challenge.route.api;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class SearchRequest {

    @NotNull
    @Min(0)
    private Integer dep_sid;

    @NotNull
    @Min(0)
    private Integer arr_sid;


    public Integer getArrSid() {
        return arr_sid;
    }

    public void setArr_sid(Integer arr_sid) {
        this.arr_sid = arr_sid;
    }

    public Integer getDepSid() {
        return dep_sid;
    }

    public void setDep_sid(Integer dep_sid) {
        this.dep_sid = dep_sid;
    }
}
