package go.euro.challenge.route.domain;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Route {
    private final int depSid;
    private final int arrSid;
    private final boolean directBusRoute;

    @JsonCreator
    Route(@JsonProperty("dep_sid") int depSid,
          @JsonProperty("arr_sid") int arrSid,
          @JsonProperty("direct_bus_route") boolean directBusRoute) {
        this.depSid = depSid;
        this.arrSid = arrSid;
        this.directBusRoute = directBusRoute;
    }

    public static Route route(int departId, int arrivalId, boolean directRoute) {
        return new Route(departId, arrivalId, directRoute);
    }

    public boolean isDirectBusRoute() {
        return directBusRoute;
    }

    public int getArrSid() {
        return arrSid;
    }

    public int getDepSid() {
        return depSid;
    }
}
