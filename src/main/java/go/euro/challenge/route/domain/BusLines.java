package go.euro.challenge.route.domain;

import com.google.common.collect.Lists;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class BusLines {
    private final List<BusLine> busLines;
    private final List<Integer> allStops;

    private BusLines(List<BusLine> busLines, List<Integer> allStops) {
        this.busLines = busLines;
        this.allStops = allStops;
    }

    public static BusLines routes(List<BusLine> busLines, List<Integer> allStops) {
        return new BusLines(unmodifiableList(busLines), unmodifiableList(allStops));
    }

    public List<BusLine> getBusLines() {
        return busLines;
    }

    public List<Integer> getAllStops() {
        return allStops;
    }

    public boolean hasStops(int departId, int arrivalId) {
        return allStops.containsAll(Lists.newArrayList(departId, arrivalId));
    }
}
