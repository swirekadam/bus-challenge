package go.euro.challenge.route.domain;

public interface DirectRouteService {
    Route findDirectConnection(int departId, int arrivalId);
}
