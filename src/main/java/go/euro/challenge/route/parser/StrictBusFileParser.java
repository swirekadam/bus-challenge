package go.euro.challenge.route.parser;


import com.google.common.base.Splitter;
import go.euro.challenge.route.domain.BusLine;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

public class StrictBusFileParser implements BusFileParser {

    public List<BusLine> parse(File file) {
        Charset charset = Charset.forName("UTF-8");
        try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
            return parseToBusLines(reader);
        } catch (IOException e) {
            throw new IOFileException(file.getAbsolutePath(), e);
        } catch (NumberFormatException e) {
            throw new InvalidFormatException(file.getAbsolutePath(), e);
        }
    }

    private List<BusLine> parseToBusLines(BufferedReader reader) throws IOException {
        int lineCount = readRouteCount(reader);
        List<BusLine> busLines = new ArrayList<>(lineCount);
        List<Integer> routesId = new ArrayList<>(lineCount);

        String line;
        while ((line = reader.readLine()) != null) {
            BusLine busLine = parseDataLine(line);
            validateRoute(routesId, busLine);
            busLines.add(busLine);
            lineCount--;
        }

        validateRoutesData(lineCount, busLines);

        return busLines;
    }

    private void validateRoutesData(int lineCount, List<BusLine> busLines) {
        if (lineCount < 0 || lineCount > 0) {
            throw new InvalidFormatException(format("There was more busLines than specified in the file header (declared: %d, has: %d)", busLines.size() + abs(lineCount), busLines.size()));
        }
    }

    private void validateRoute(List<Integer> routesId, BusLine busLine) {
        if (routesId.contains(busLine.getId())) {
            throw new InvalidFormatException(format("Duplicated busLine id: %d", busLine.getId()));
        }
        routesId.add(busLine.getId());
    }

    private BusLine parseDataLine(String line) {
        List<Integer> ids = Splitter
                .on(" ")
                .splitToList(line).stream()
                .map(Integer::parseInt)
                .collect(toList());

        return BusLine.route(ids.get(0), ids.subList(1, ids.size()));
    }

    private int readRouteCount(BufferedReader reader) throws IOException {
        String header = reader.readLine();
        return Integer.parseInt(header);
    }
}
