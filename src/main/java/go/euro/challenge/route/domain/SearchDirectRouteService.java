package go.euro.challenge.route.domain;

import java.util.List;
import java.util.Optional;

import static go.euro.challenge.route.domain.Route.route;

public class SearchDirectRouteService implements DirectRouteService {

    private final List<BusLine> busLines;

    public SearchDirectRouteService(List<BusLine> busLines) {
        this.busLines = busLines;
    }

    public Route findDirectConnection(int departId, int arrivalId) {
        Optional<BusLine> directRoute = busLines.stream()
                .filter(route -> route.hasDirectConnection(departId, arrivalId))
                .findAny();

        return route(departId, arrivalId, directRoute.isPresent());
    }
}
